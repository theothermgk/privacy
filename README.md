`Last Updated: November 19 2019`  

This list contains all the services and applications discussed in the 'Intro to OpSec' presentation and some more!  

---  


# PREFACE:  

This document is a starting point. It contains links to popular recommended options for most of your security and privacy needs. Critical information has been omitted due to time constraints. The responsibility is delegated to you to do your own research before using any of the services mentioned below.  
  
I am not responsible for any damages resulting from the use of the information contained within this document.  

# Software  

### **VPN**  
`ProtonVPN`             (https://protonvpn.com)  
`Librem Tunnel`         (https://librem.one/)  
`ExpressVPN *`          (https://www.expressvpn.com/)  
>\* ExpressVPN has a really big marketing campaign, and they get their sponsored parties to lie about VPNs. Tread carefully!  
> NordVPN was removed because not only do they do 'this.marketing * 1000' they were recently all over the news for not disclosing a "hack".  

### **Web Browsers**  
`Firefox`       		(https://www.mozilla.org/en-US/firefox/new/)  
`Tor`   				(https://www.torproject.org/download/download.html.en)  

### **Search Engines**  
`DuckDuckGo` 		    (https://duckduckgo.com)  
`StartPage`             (https://startpage.com)  

### **Encrypted Calls and Messaging**  
`Signal`    	        (https://signal.org/download/)  
> The desktop client for Signal requires a synced mobile account.  

### **Encrypted E-mail**  
`Tutanota`              (https://tutanota.com/)  
`ProtonMail`            (https://protonmail.com)  

### **Password Managers**  
`KeePassXC`             (https://keepassxc.org)  

### **Data Encryption**  
`VeraCrypt`	    		(https://www.veracrypt.fr/en/Downloads.html)  

### **Operating Systems**  
`QubeOS`                (https://www.qubes-os.org)  
`Tails`                 (https://tails.boum.org)  
`Whonix`                (https://www.whonix.org)  
`Kali Linux *`          (https://www.kali.org)  
>\* Kali is a PenTest oriented distribution. It has some interesting default settings like not turning on services automatically on boot. It's all detailed in the Kali Linux Book linked below. You can learn from and implement some of Kali's way-of-life in your own distro if you choose not to use specialised distributions like QubeOS!  

# Devices

### **Phones**  
`Librem 5`              (https://puri.sm/products/librem-5/)  
`Pinephone`             (https://www.pine64.org/pinephone/)

### **Laptops**  
`Librem 13,15`          (https://puri.sm/products/)  
> I wont link to old refurbished IBM's like the Thinkpad x230 etc, just know they are available with a custom open-source BIOS LibreBoot/CoreBoot if you are interested.  

### **Various Hardware**
`Computer parts`        (https://ryf.fsf.org/products)


# Going in-depth 
Firefox  
---
### **Tweaks**  
 `about:config` 	    (https://rationallyparanoid.com/articles/firefox-about-config-security.html)  
 `Preferences` 			(https://geekeasier.com/optimize-mozilla-firefox-for-maximum-security-and-privacy/11311/)  
 
### **Add-ons**  
> Also applicable for the Android version of Firefox.  

 `NoScript`         Control Javascript execution.                                   (https://addons.mozilla.org/en-US/firefox/addon/noscript/)  
 `Decentraleyes`    Blocks third party trackers in an intuitive way.                (https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)  
 `uBlock Origin`    Block advertisements, control web elements, host lists, etc.  	(https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)  
 `uMatrix` 	        Control matrix for web elements & third party sites.            (https://addons.mozilla.org/en-US/firefox/addon/umatrix/)  
 `HttpsEverywhere`  Force https protocol when available.	                        (https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)  
 `Cookie AutoDelete` Delete cookies and manage cookie deletion rules                (https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)  
 `Multi-Account Containers` Keep cookies isolated within tab groups                 (https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)  

Windows  
---  
### **Windows Utilities**  
`ShutUp10` Extensive tool for disabling services with privacy concerns for Windows 10. (https://www.oo-software.com/en/shutup10)  

### **Windows Registry Tweaks**  
> Search the web for many more tweaks!  

`Disable Active-Probing` Stop automatic IPv4/6 pings to dns.msftncsi.com (https://www.trishtech.com/2015/07/disable-active-internet-probing-ncsi-in-windows/)  
`Disable WifiSense` Prevent the sharing of Wifi passwords and hotspot auto-connect (https://www.thewindowsclub.com/disable-wi-fi-sense-windows-10-enterprise)  
`Take Ownership` Context menu to gain file permissions. (https://www.tenforums.com/tutorials/3841-add-take-ownership-context-menu-windows-10-a.html)  

### **Compilations**  
`Block Windows` Read the documentation. Some tweaks are version specific. (https://github.com/WindowsLies/BlockWindows/)  
`Secure Windows 10` Some important information. (https://www.zdnet.com/article/how-to-secure-windows-10-the-paranoids-guide/)  
  

Android  
---
> Stock ROMs have severe limitations and pitfalls when it comes to security and privacy. Using AOSP or similar is recommended.  

### **ROMS**
`Replicant`             (https://replicant.us/)

### **Android Apps**  
> Apps that require root are labeled. Apps (APKs) that are not available on F-Droid can often be found on their official websites, etc.  

`F-Droid` Open-source web store alternative to Google PlayStore. (https://f-droid.org/)  
`AFWall+` Firewall (Root)                             (https://play.google.com/store/apps/details?id=dev.ukanth.ufirewall)  
`Signal` Encrypted SMS messaging.                     (https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms)  
`Orbot` Tor client for Android (Root recommended)     (https://play.google.com/store/apps/details?id=org.torproject.android)  
`Orfox`  Firefox Browser for Orbot                    (https://play.google.com/store/apps/details?id=info.guardianproject.orfox)  
`Haven` Turn your phone into a surveillance system!   (https://play.google.com/store/apps/details?id=org.havenapp.main&hl=en)  

### **Xposed Framework** (Root)  
Official site (https://repo.xposed.info/)  
XDA Forum (https://forum.xda-developers.com/xposed)  
  
> All modules are downloaded directly from the Xposed Manager App.  

`Adblock Reborn` Block advertisements system-wide.	        (https://forum.xda-developers.com/xposed/modules/xposed-adblocker-reborn-1-0-1-2017-02-11-t3554617)  
`Unbeloved Hosts` Downloadable host lists 			        (https://www.xda-developers.com/unbelovedhosts-uses-xposed-to-block-unwanted-connections/)  
`XPrivacy` In-depth control of permissions and much more.   (https://forum.xda-developers.com/xposed/modules/xprivacy-ultimate-android-privacy-app-t2320783)  
`XPrivacyLua` For Android 6.0+                              (https://forum.xda-developers.com/xposed/modules/xprivacylua6-0-android-privacy-manager-t3730663)  

# Appendix  

## Random documentation, links and videos  

### Links  
`Panopticlick` Test your web browser against certain tracking methods! (https://panopticlick.eff.org/)  
`Clickclickclick` Interactive study/game on user profiling! (https://clickclickclick.click/)  
`OverTheWire` CTF games to practice your skills and learn new tricks! (http://overthewire.org/wargames/)  

### Documentation  
`QubeOS, Tails, Whonix` (https://www.comparitech.com/blog/vpn-privacy/anonymity-focused-linux-distributions/)  
`Kali Linux Free eBook` (https://www.kali.org/download-kali-linux-revealed-book/)  
`Compartmentalization Theory`  (https://en.wikipedia.org/wiki/Compartmentalization_(information_security))  
`Common Mobile Security Problems` (https://www.pcworld.com/article/2010278/10-common-mobile-security-problems-to-attack.html)  
  
`Tor FAQ` (https://www.torproject.org/docs/faq.html.en)  
`Tor + VPN` (https://trac.torproject.org/projects/tor/wiki/doc/TorPlusVPN)  
  
`Whonix Documentation` (https://www.whonix.org/wiki/Documentation)  

### Videos  
`LiveOverflow` Computer security. (https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)  
`John Hammond` Computer security. (https://www.youtube.com/channel/UCVeW9qkBjo3zosnqUbG7CFw)  
`danooct1` Revisiting old computer viruses! (https://www.youtube.com/user/danooct1)  
